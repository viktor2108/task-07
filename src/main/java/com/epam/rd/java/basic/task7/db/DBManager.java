package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    static Properties properties = new Properties();

    private static String CONNECTION_URL;

    static {
        try {
            properties.load(new FileInputStream("app.properties"));
            CONNECTION_URL = properties.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static DBManager instance = new DBManager();

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        Connection connection = null;
        Statement stmt;
        ResultSet rs;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            stmt = connection.createStatement();
            rs = stmt.executeQuery(Commands.SQL_FIND_ALL_USERS.getTitle());
            while (rs.next()) {
                users.add(extractUser(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot find all users", e);
        } finally {
            close(connection);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        Connection connection = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            pstmt = connection.prepareStatement(Commands.SQL_CREATE_USER.getTitle(),
                    Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, user.getLogin());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot insert user", e);
        } finally {
            close(connection);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection connection = null;
        PreparedStatement pstmt;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            connection.setAutoCommit(false);
            pstmt = connection.prepareStatement(Commands.SQL_DELETE_USER.getTitle());

            for (User user : users) {
                pstmt.setInt(1, user.getId());
                pstmt.addBatch();
            }
            int[] deleteUsers = pstmt.executeBatch();
            for (int k : deleteUsers) {
                if (k != 1) {
                    return false;
                }
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot delete users", e);
        } finally {
            close(connection);
        }
    }

    public User getUser(String login) throws DBException {
        User user = null;
        Connection connection = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            pstmt = connection.prepareStatement(Commands.SQL_FIND_USER_BY_LOGIN.getTitle());
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                user = extractUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot get user", e);
        } finally {
            close(connection);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        Connection connection = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            pstmt = connection.prepareStatement(Commands.SQL_FIND_TEAM_BY_NAME.getTitle());
            pstmt.setString(1, name);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                team = extractTeam(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot get team", e);
        } finally {
            close(connection);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        Connection connection = null;
        Statement stmt;
        ResultSet rs;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            stmt = connection.createStatement();
            rs = stmt.executeQuery(Commands.SQL_FIND_ALL_TEAMS.getTitle());
            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot find all teams", e);
        } finally {
            close(connection);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            pstmt = connection.prepareStatement(Commands.SQL_CREATE_TEAM.getTitle(),
                    Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, team.getName());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot insert team", e);
        } finally {
            close(connection);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        PreparedStatement pstmt;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            connection.setAutoCommit(false);
            pstmt = connection.prepareStatement(Commands.SQL_CREATE_USER_TEAM.getTitle());
            for (Team team : teams) {
                pstmt.setInt(1, user.getId());
                pstmt.setInt(2, team.getId());
                pstmt.executeUpdate();
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(connection);
            throw new DBException("Cannot set teams", e);
        } finally {
            close(connection);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        Connection connection = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            pstmt = connection.prepareStatement(Commands.SQL_FIND_TEAMS_BY_USER_ID.getTitle());
            pstmt.setInt(1, user.getId());
            rs = pstmt.executeQuery();
            while (rs.next()) {
                teams.add((getTeamByID(rs.getInt("team_id"))));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot get user_teams", e);
        } finally {
            close(connection);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement pstmt;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            pstmt = connection.prepareStatement(Commands.SQL_DELETE_TEAM.getTitle());
            pstmt.setInt(1, team.getId());
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot delete team", e);
        } finally {
            close(connection);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement pstmt;

        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            pstmt = connection.prepareStatement(Commands.SQL_UPDATE_TEAM.getTitle());

            int i = 1;
            pstmt.setString(i++, team.getName());
            pstmt.setInt(i++, team.getId());

            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot update team", e);
        } finally {
            close(connection);
        }
    }

    public Team getTeamByID(int id) throws DBException {
        Team team = null;
        Connection connection = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            pstmt = connection.prepareStatement(Commands.SQL_FIND_TEAM_BY_ID.getTitle());
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                team = extractTeam(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot get team", e);
        } finally {
            close(connection);
        }
        return team;
    }

    private void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }

    private Team extractTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }

    private void rollback(Connection connection) {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
