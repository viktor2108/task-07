package com.epam.rd.java.basic.task7.db;

public enum Commands {
    SQL_FIND_ALL_USERS("select * from users"),

    SQL_FIND_USER_BY_LOGIN("select * from users where login=?"),

    SQL_CREATE_USER("insert into users values (default, ?)"),

    SQL_DELETE_USER("delete from users where id =?"),

    SQL_FIND_ALL_TEAMS("select * from teams"),

    SQL_FIND_TEAM_BY_NAME("select * from teams where name = ?"),

    SQL_CREATE_TEAM("insert into teams values (default, ?)"),

    SQL_UPDATE_TEAM("update teams set name=? where id=?"),

    SQL_DELETE_TEAM("delete from teams where id =?"),

    SQL_CREATE_USER_TEAM("insert into users_teams values (?,?)"),

    SQL_FIND_TEAMS_BY_USER_ID("select * from users_teams where user_id = ?"),

    SQL_FIND_TEAM_BY_ID("select * from teams where id = ?");

    private String title;

    Commands(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
